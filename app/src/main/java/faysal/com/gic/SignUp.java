package faysal.com.gic;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class SignUp extends AppCompatActivity implements View.OnClickListener {

    EditText getEmail;
    EditText getPassword;
    EditText getName;
    EditText getPhone;
    Button newAccount;
    TextView logInBtn;
    Typeface typeface;
    Typeface typeface2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        getEmail=(EditText)findViewById(R.id.getEmail);
        getPassword=(EditText)findViewById(R.id.getPassword);
        getName=(EditText)findViewById(R.id.getName);
        getPhone=(EditText)findViewById(R.id.getPhone);


        typeface=Typeface.createFromAsset(getAssets(),"fonts/sang.ttf");
        typeface2=Typeface.createFromAsset(getAssets(),"fonts/Tekton.otf");

        logInBtn=(TextView)findViewById(R.id.logInBtn);
        newAccount=(Button)findViewById(R.id.signUpBtn);

        logInBtn.setTypeface(typeface2);
        newAccount.setTypeface(typeface2);

        getEmail.setTypeface(typeface);
        getName.setTypeface(typeface);
        getPassword.setTypeface(typeface);
        getPhone.setTypeface(typeface);


        newAccount.setOnClickListener(this);

        logInBtn.setOnClickListener(this);


    }


    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.signUpBtn:
                createAccount();
                break;

            case R.id.logInBtn:
                logInOperation();
                break;



        }
    }

    private void createAccount(){

        if (getName.getText().toString().trim().isEmpty()){
            getName.setError("Name is requred");
            getName.requestFocus();
            return;
        }

        if (getEmail.getText().toString().trim().isEmpty()){
            getEmail.setError("Email is requred");
            getEmail.requestFocus();
            return;
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(getEmail.getText().toString().trim()).matches()){
            getEmail.setError("Enter valid email");
            getEmail.requestFocus();
            return;
        }

        if (getPhone.getText().toString().trim().isEmpty()){
            getPhone.setError("Phone is requred");
            getPhone.requestFocus();
            return;
        }

        if (getPassword.getText().toString().isEmpty()){
            getPassword.setError("Password is requred");
            getPassword.requestFocus();
            return;
        }

        if (getPassword.getText().toString().length() <6){
            getPassword.setError("password should be 6 cherector long");
            getPassword.requestFocus();
            return;
        }

        HelperClass data=new HelperClass(
                getName.getText().toString().trim(),
                getEmail.getText().toString().trim(),
                getPhone.getText().toString().trim(),
                getPassword.getText().toString().trim()
        );

        DbOperation operation=new DbOperation(getApplicationContext());

        if (operation.isEmailExists(getEmail.getText().toString().trim())){
            getEmail.setError("Email Already exists");
            getEmail.requestFocus();
            return;

        }
        boolean result=operation.insertIntoDatabase(data);

        if (result){

            Intent intent=new Intent(getApplicationContext(),UserView.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.putExtra("name",getName.getText().toString().trim());
            intent.putExtra("phone",getPhone.getText().toString().trim());
            intent.putExtra("email",getEmail.getText().toString().trim());
            intent.putExtra("password",getPassword.getText().toString());
            intent.putExtra("message","Registration successfully ! ");
            intent.putExtra("result",false);
            startActivity(intent);

        }else {
            Toast.makeText(this, "Registration Failed ! You can try again", Toast.LENGTH_SHORT).show();
        }

    }

    private void logInOperation(){
        startActivity(new Intent(getApplicationContext(),MainActivity.class));
    }

}
