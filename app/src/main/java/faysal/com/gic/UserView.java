package faysal.com.gic;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class UserView extends AppCompatActivity {


    Typeface typeface;
    Typeface typeface2;
    Typeface typeface3;

    TextView name;
    TextView email;
    TextView phone;
    TextView password;
    TextView showMessage;

    Button logOut;

    String getName,getEmail,getPassword,getPhone,getMsg;
    boolean getResult;

    LinearLayout mainLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_view);


        typeface=Typeface.createFromAsset(getAssets(),"fonts/sang.ttf");
        typeface2=Typeface.createFromAsset(getAssets(),"fonts/Tekton.otf");
        typeface3=Typeface.createFromAsset(getAssets(),"fonts/Almendra.ttf");

        mainLayout=(LinearLayout)findViewById(R.id.mainLayout);

        name=(TextView)findViewById(R.id.name);
        email=(TextView)findViewById(R.id.email);
        password=(TextView)findViewById(R.id.password);
        phone=(TextView)findViewById(R.id.phone);
        showMessage=(TextView)findViewById(R.id.showMessage);

        logOut=(Button)findViewById(R.id.logOut);

        name.setTypeface(typeface2);
        logOut.setTypeface(typeface2);
        showMessage.setTypeface(typeface3);

        email.setTypeface(typeface);
        password.setTypeface(typeface);
        phone.setTypeface(typeface);

        getName=getIntent().getStringExtra("name");
        getEmail=getIntent().getStringExtra("email");
        getPassword=getIntent().getStringExtra("password");
        getPhone=getIntent().getStringExtra("phone");
        getMsg=getIntent().getStringExtra("message");

        getResult=getIntent().getBooleanExtra("result",false);

        if (getResult==false){
            mainLayout.setVisibility(View.GONE);
            logOut.setText("Log in Account");
        }

        if (!getName.isEmpty()){
            name.setText(getName);
        }

        if (!getPhone.isEmpty()){
            phone.setText(getPhone);
        }


        if (!getEmail.isEmpty()){
            email.setText(getEmail);
        }

        if (!getPassword.isEmpty()){
            password.setText(getPassword);
        }


        if (!getName.isEmpty()){
            showMessage.setText(getMsg);
        }

        logOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent=new Intent(getApplicationContext(),MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });

    }
}
