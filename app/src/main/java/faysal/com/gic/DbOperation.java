package faysal.com.gic;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DbOperation extends SQLiteOpenHelper {

    private static final String DB_NAME="mydb";
    private static final String TABLE_NAME="ap_Rec";
    private static final String ID="id";
    private static final String NAME="name";
    private static final String EMAIL="mydb";
    private static final String PASSWORD="password";
    private static final String PHONE="phone";


    private static final String TAG = "DATABASE_OPERATION";


    private static final String CREATE_QUERY="CREATE TABLE "
            +TABLE_NAME+"("+ID+" INTEGER PRIMARY KEY AUTOINCREMENT ,"
            +NAME+" TEXT, "+EMAIL+" TEXT, "
            +PHONE+" TEXT, "+PASSWORD+" TEXT "+")";


    public DbOperation(Context context) {
        super(context, DB_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_QUERY);

    }


    public boolean insertIntoDatabase(HelperClass data){
        SQLiteDatabase database=getWritableDatabase();
        ContentValues values=new ContentValues();
         values.put(NAME,data.getName());
         values.put(EMAIL,data.getEmail());
         values.put(PHONE,data.getPhone());
         values.put(PASSWORD,data.getPassword());

         long result=database.insert(TABLE_NAME,null,values);


        if (result == -1) {
            Log.d(TAG, "Error inserting ");
            return false;
        } else {
            Log.d(TAG, "Successfully inserting");
            return true;
        }
    }

    public boolean isEmailExists(String email) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_NAME,// Selecting Table
                new String[]{ID, NAME, EMAIL,PHONE, PASSWORD},//Selecting columns want to query
                EMAIL + "=?",
                new String[]{email},//Where clause
                null, null, null);

        if (cursor != null && cursor.moveToFirst()&& cursor.getCount()>0) {
            //if cursor has value then in user database there is user associated with this given email so return true
            return true;
        }
        //if email does not exist return false
        return false;
    }

    public HelperClass userLogIN(String email, String password) {
        SQLiteDatabase db = this.getReadableDatabase();
        HelperClass msinUser = null;
        Cursor cursor = db.query(TABLE_NAME,// Selecting Table
                new String[]{ID, NAME, EMAIL,PHONE,PASSWORD},//Selecting columns want to query
                EMAIL + "=?",
                new String[]{email},//Where clause
                null, null, null);



        if (cursor != null && cursor.moveToFirst()&& cursor.getCount()>0) {

            msinUser = new HelperClass(cursor.getString(cursor.getColumnIndex(NAME))
                    , cursor.getString(cursor.getColumnIndex(EMAIL))
                    , cursor.getString(cursor.getColumnIndex(PHONE))
                    , cursor.getString(cursor.getColumnIndex(PASSWORD)));

            //Match both passwords check they are same or not
            if (password.equalsIgnoreCase(msinUser.password)) {
                //Log.d(TAG, "Authenticate: "+msinUser.name+" "+msinUser.email+" "+msinUser.phone+" "+msinUser.password);
                return msinUser;
            }
         }

        //Log.d(TAG, "Authenticate: "+msinUser.name+" "+msinUser.email+" "+msinUser.phone+" "+msinUser.password);

        return null;
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
